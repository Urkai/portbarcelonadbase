/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.boscdelacoma.portbarcelona.views.ui.javafx;

import cat.boscdelacoma.portbarcelona.views.ui.test.PortBarcelona;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author marc
 */
public class JavaFXApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        
        try {
            Parent root = FXMLLoader.load(getClass().getResource("fxml/portfxml.fxml"));
            
            Scene scene = new Scene(root, 800, 600);
            
            primaryStage.setTitle("PORT DE BARCELONA");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException ex) {
            Alert dialog  = new Alert(Alert.AlertType.ERROR);
            dialog.setTitle("Error d'entrada/sortida");
            dialog.setContentText(ex.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
