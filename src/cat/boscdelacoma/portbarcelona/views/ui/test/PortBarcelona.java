package cat.boscdelacoma.portbarcelona.views.ui.test;

import cat.boscdelacoma.portbarcelona.model.business.entities.Cisterna;
import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.DryVan;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.business.entities.Refrigerat;
import cat.boscdelacoma.portbarcelona.model.business.entities.Vaixell;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.ContenidorDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.MercaderiaDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.db4o.ContenidorDb4oDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.db4o.MercaderiaD4oDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.jdbc.mysql.ContenidorMySQLDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.jdbc.mysql.MercaderiaMySQLDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PortBarcelona {

    public static void carregarVaixell(Vaixell vaixell) {

        //createDB4oObjects();
        //System.out.println("BD CREADA!");

        // crear instàncies de les classes que permeten accedir al sistema de 
        // persistència amb fitxers de dades
        //MercaderiaDAO mercaderiaDAO = new MercaderiaFilesDAO();
        //ContenidorDAO contenidorDAO = new ContenidorFilesDAO();
        // crear instàncies de les classes que permeten accedir al sistema de 
        // persistència amb bases de dades (MySQL)
        MercaderiaDAO mercaderiaDAO = new MercaderiaMySQLDAO(); // MercaderiaMySQLDAO();
        
        
        
        
        ContenidorDAO contenidorDAO = new ContenidorMySQLDAO();// ContenidorMySQLDAO();

        try {
            // obtenir tots els contenidors emmagatzemats
            Map<String, Contenidor> totsContenidors = contenidorDAO.getContenidors();
            // obtenir tres contenidors de la llista
            Contenidor c1 = totsContenidors.get("D9873456");
            Contenidor c2 = totsContenidors.get("C4562345");
            Contenidor c3 = totsContenidors.get("R7638493");

            // obrir el contenidor per poder afegir-hi mercaderies
            c1.obrir();
            afegirMercaderies(c1, mercaderiaDAO.getMercaderiesFromContainer(c1));
            c1.tancar();
            // carregar el contenidor al vaixell
            vaixell.addContenidor(c1);

            c2.obrir();
            afegirMercaderies(c2, mercaderiaDAO.getMercaderiesFromContainer(c2));
            c2.tancar();
            vaixell.addContenidor(c2);

            c3.obrir();
            afegirMercaderies(c3, mercaderiaDAO.getMercaderiesFromContainer(c3));
            c3.tancar();
            vaixell.addContenidor(c3);

        } catch (DAOException ex) {
            System.out.println(ex.getMessage());
        } catch (UnsupportedOperationException ex) {
            System.out.println(ex.getMessage());
        }
    }

    static void afegirMercaderies(Contenidor contenidor, List<Mercaderia> mercaderies) {
        if (mercaderies != null) {
            mercaderies.forEach(mercaderia -> {
                contenidor.addMercaderia(mercaderia);
            });
        }
    }

    static void createDB4oObjects() {
        ContenidorDb4oDAO con = new ContenidorDb4oDAO();
        MercaderiaD4oDAO merc = new MercaderiaD4oDAO();

        Map<String, Contenidor> contenidors = new TreeMap<>();

        Contenidor c1 = new Cisterna("C4562345", 1200.0f, true);
        Contenidor c2 = new DryVan("D9873456", 100.0f, true, "BLAU");
        Contenidor c3 = new Refrigerat("R7638493", 1000.0f, true, -14.0f);

        contenidors.put(c1.getNumSerie(), c1);
        contenidors.put(c2.getNumSerie(), c2);
        contenidors.put(c3.getNumSerie(), c3);

        List<Mercaderia> mercaderies = new ArrayList<>();
        mercaderies.add(new Mercaderia("Teclat", 20, c2));
        mercaderies.add(new Mercaderia("Formatge", 30, c3));
        mercaderies.add(new Mercaderia("Gasolina", 1000, c1));

        c2.addMercaderia(mercaderies.get(0));
        c3.addMercaderia(mercaderies.get(1));
        c1.addMercaderia(mercaderies.get(2));
        c2.tancar();
        c3.tancar();
        c1.tancar();

        try {
            con.saveContenidors(contenidors);
        } catch (DAOException ex) {
            System.out.println(ex);
        }

        try {
            merc.saveMercaderies(mercaderies);
        } catch (DAOException ex) {
            Logger.getLogger(PortBarcelona.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
