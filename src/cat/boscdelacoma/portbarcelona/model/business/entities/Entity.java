

package cat.boscdelacoma.portbarcelona.model.business.entities;

import cat.boscdelacoma.portbarcelona.model.business.utilities.NumberUtils;

/**
 * Tipus de dades Abstracte (genèric) que proporcionarà un comportament comú
 * a totes les entitats de la capa de negoci.
 * 
 * @author Marc Nicolau
 */
public abstract class Entity {
    
    //<editor-fold defaultstate="collapsed" desc="Atributs / Camps">
    //<editor-fold defaultstate="collapsed" desc="Estat: atributs d'instància">
    private long id = NumberUtils.UNSAVED_VALUE;
    //</editor-fold>
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Comportament: Mètodes / Operacions">
    //<editor-fold defaultstate="collapsed" desc="Mètodes d'instància">
    //<editor-fold defaultstate="collapsed" desc="Getters / Setters">
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        // primera restricció: no es pot assignar valor
        if(getId() !=  NumberUtils.UNSAVED_VALUE) {
           throw new UnsupportedOperationException("id cannot be changed");
        }
        // segona restricció: que el valor no sigui zero o menor
        if(id <= NumberUtils.ZERO) {
           throw new IllegalArgumentException("id cannot be negative or zero");
        }
        this.id = id;
    }
    //</editor-fold>
    //</editor-fold>
    //</editor-fold>

}