package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.db4o;

import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Classe que implementa el patró Singleton per obtenir la connexió a la BD.
 *
 * @author Usuario
 */
public class DB4OConnection {

    private final String FILE_CONFIG = "resources/configDb4o.properties";

    private static DB4OConnection instance;
    private ObjectContainer connection;

    private DB4OConnection() throws DAOException {

        Properties prop = new Properties();

        try (InputStream inputStream = new FileInputStream(FILE_CONFIG)) {
            if (inputStream != null) {
                prop.load(inputStream);
                String database = prop.getProperty("database");

                connection = Db4oEmbedded.openFile(database);

            } else {
                throw new DAOException("No es pot llegir la configuració de connexió amb la base de dades. Fitxer '" + FILE_CONFIG + "' no s'ha trobat");
            }
        } catch (IOException ex) {
            throw new DAOException("No es pot obrir la base de dades");
        }

    }

    public ObjectContainer getConnection() {
        return connection;
    }

    public static DB4OConnection getInstance() throws DAOException {

        if (instance == null) {
            instance = new DB4OConnection();
        }

        return instance;

    }

}
