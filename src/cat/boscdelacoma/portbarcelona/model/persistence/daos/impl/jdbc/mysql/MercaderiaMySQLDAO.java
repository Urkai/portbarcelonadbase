package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.jdbc.mysql;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.MercaderiaDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import cat.boscdelacoma.portbarcelona.model.persistence.utilities.JDBCUtils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MercaderiaMySQLDAO implements MercaderiaDAO {

    @Override
    public List<Mercaderia> getMercaderies() throws DAOException {

        List<Mercaderia> list = new ArrayList<>();

        // TODO: obtenir totes les mercaderies que hi ha a la BD MySQL
    Mercaderia mercaderia = null;

        try ( 
                PreparedStatement query = MySQLConnection.getInstance().
                        getConnection().
                        prepareStatement("SELECT id, description, volume, numseriecontainer " + "FROM mercaderia")) { 
                        try (ResultSet reader = query.executeQuery();) {
                            while (reader.next()) {
                                mercaderia = JDBCUtils.getMercaderia(reader);
                                list.add(mercaderia);
                            }
                                }catch (Exception ex) {
                                 throw new DAOException("Error en accedir a la taula de Mercaderies " + ex);
                                }
                
             } catch (SQLException ex) {
                    throw new DAOException("Error en accedir a la taula de Mercaderies" + ex);
        
            }
        return list;
    } 
        
    @Override
    public List<Mercaderia> getMercaderiesFromContainer(Contenidor container) throws DAOException {
        List<Mercaderia> list = new ArrayList<>();

        // TODO: obtenir totes les mercaderies de la BD  MySQL que estan dins el contenidor passat per paràmetre.
                try (
                 PreparedStatement query = MySQLConnection.getInstance().
                        getConnection().
                        prepareStatement("SELECT * FROM mercaderia WHERE numseriecontainer = ?")) {
                        query.setString(1, container.getNumSerie());
                    try ( ResultSet rs = query.executeQuery();) {

                        while (rs.next()) {
                            list.add(JDBCUtils.getMercaderia(rs));
                        }
                    } catch (Exception ex) {
                        throw new DAOException("Error en accedir a la taula de Mercaderies " + ex);
                    }
                } catch (SQLException ex) {
                    throw new DAOException("Error en accedir a la taula de Mercaderies " + ex);
                }
                
        return list;
    }

    @Override
    public void saveMercaderies(List<Mercaderia> mercaderies) throws DAOException {

        // TODO: desar totes les mercaderies de la List, dins la BD MySQL
        try (
                PreparedStatement query = MySQLConnection.getInstance().
                        getConnection().
                         prepareStatement("INSERT INTO mercaderia "
                                + "VALUES(?, ?, ?, ?")) {
                        try {
                        MySQLConnection.getInstance().getConnection().setAutoCommit(false); 
                        
                        for (Mercaderia mercaderia : mercaderies) {
                            query.setLong(1, mercaderia.getId());
                            query.setString(2, mercaderia.getDescripcio());
                            query.setFloat(3, mercaderia.getVolum());
                            query.setString(4, mercaderia.getContenidor().getNumSerie());
                        }
                        query.executeUpdate();
                        MySQLConnection.getInstance().getConnection().commit();

                    } catch (Exception ex) {
                        // Lloc on fer el Rollback en cas de ser necessari
                        MySQLConnection.getInstance().getConnection().rollback();
                        throw new DAOException("Error en accedir a la taula de Mercaderies");
                    } finally {
                        MySQLConnection.getInstance().getConnection().setAutoCommit(true);
                    }
                } catch (SQLException ex) {
                    throw new DAOException("Error en accedir a la taula de Mercaderies");
                }
    }
}
