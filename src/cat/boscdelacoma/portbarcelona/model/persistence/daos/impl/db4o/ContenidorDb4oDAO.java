package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.db4o;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;

import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.ContenidorDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import com.db4o.ObjectSet;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Marc Nicolau
 */
public class ContenidorDb4oDAO implements ContenidorDAO {

    @Override
    public Contenidor getContenidorByNumSerie(String numSerie) throws DAOException {
        Contenidor contenidor = null;
        ObjectSet<Contenidor> results = DB4OConnection.getInstance().getConnection().queryByExample(new Contenidor(numSerie, 0, false));
        if (results.size() == 0) {
            // si no ha trobat amb el 3r paràmetre a false, provem amb true (els booleans no es descarten en les queryByExample)
            results = DB4OConnection.getInstance().getConnection().queryByExample(new Contenidor(numSerie, 0, true));
        }
        if (results.size() > 0) {
            contenidor = results.next();
        }
        //DB4OConnection.getInstance().getConnection().close();
        return contenidor;
    }

    @Override
    public Map<String, Contenidor> getContenidors() throws DAOException {
        Map<String, Contenidor> map = new TreeMap<>();
        ObjectSet<Contenidor> results = DB4OConnection.getInstance().getConnection().queryByExample(Contenidor.class);
        for (Contenidor contenidor : results) {
            map.put(contenidor.getNumSerie(), contenidor);

        }
        //DB4OConnection.getInstance().getConnection().close();

        return map;
    }

    @Override
    public void saveContenidors(Map<String, Contenidor> contenidors) throws DAOException {

        for (Map.Entry<String, Contenidor> entry : contenidors.entrySet()) {
            DB4OConnection.getInstance().getConnection().store(entry.getValue());
        }
        //DB4OConnection.getInstance().getConnection().close();

    }

}
