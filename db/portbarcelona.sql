CREATE DATABASE IF NOT exists `portbarcelona`;

USE container `portbarcelona`;

DROP TABLE IF EXISTS `container`;

CREATE TABLE `container` (
  `serialnumber` varchar(10) NOT NULL,
  `capacity` double DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `minTemp` double DEFAULT NULL,
  `color` varchar(30) DEFAULT NULL,
  `estat` tinyint DEFAULT '0',
  PRIMARY KEY (`serialnumber`)
); 

--
-- Dumping data for table `container`
--

LOCK TABLES `container` WRITE;

INSERT INTO `container` VALUES ('C4562345',1200,'Cisterna',NULL,NULL,0),('D9873456',100,'DryVan',NULL,NULL,0),('R7638493',1000,'Refrigerat',-14,NULL,0);

UNLOCK TABLES;

--
-- Table structure for table `mercaderia`
--

DROP TABLE IF EXISTS `mercaderia`;

CREATE TABLE `mercaderia` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(128) DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `numseriecontainer` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `container_serial_idx` (`numseriecontainer`),
  CONSTRAINT `container_serial` FOREIGN KEY (`numseriecontainer`) REFERENCES `container` (`serialnumber`) ON UPDATE CASCADE
);

--
-- Dumping data for table `mercaderia`
--

LOCK TABLES `mercaderia` WRITE;

INSERT INTO `mercaderia` VALUES (1,'Teclat',20,'D9873456'),(2,'Formatge',30,'R7638493'),(3,'Gasolina',1000,'C4562345');

UNLOCK TABLES;
